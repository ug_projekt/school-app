package com.example.application.school.base.DateUtiils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class DateUtils {

    private static final String DatePattern = "ddMMyyyy";
    private static final String DateFormatter = "dd-MM-yyyy";

    public static LocalDate convertString2LocalDate(String stringDate){

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DatePattern);
        if(stringDate != null || stringDate.isEmpty()){
            return LocalDate.parse(stringDate, formatter);
        }else{
            return null;
        }
    }

    public static String convertLocalDate2String(LocalDate date){
        return Optional.ofNullable(date).map(d -> d.format(DateTimeFormatter.ofPattern(DateFormatter))).orElse(null);
    }
}
