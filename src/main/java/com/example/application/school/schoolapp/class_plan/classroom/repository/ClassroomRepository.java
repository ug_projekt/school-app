package com.example.application.school.schoolapp.class_plan.classroom.repository;

import com.example.application.school.schoolapp.class_plan.classroom.model.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassroomRepository extends JpaRepository<Classroom, Long> {

    Classroom findByNumber(String number);
}
