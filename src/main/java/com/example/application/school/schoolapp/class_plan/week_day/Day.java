package com.example.application.school.schoolapp.class_plan.week_day;

public enum Day {
    MONDAY(1L, "Monday"),
    TUESDAY(2L, "Tuesday"),
    WEDNESDAY(3L, "Wednesday"),
    THURSDAY(4L, "Thursday"),
    FRIDAY(5L, "Friday");

    private Long dayValue;
    private String dayName;


    Day(Long dayValue, String dayName) {
        this.dayName = dayName;
        this.dayValue = dayValue;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public Long getDayValue() {
        return dayValue;
    }

    public void setDayValue(Long dayValue) {
        this.dayValue = dayValue;
    }

    public static Day getBydayValue(Long dayValue){
        for(Day day : values()){
            if(day.dayValue.equals(dayValue)){
                return day;
            }
        } throw new IllegalArgumentException();
    }
}
