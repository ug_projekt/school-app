package com.example.application.school.schoolapp.student.service;

import com.example.application.school.base.DateUtiils.DateUtils;
import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_entity.service.ClassEntityService;
import com.example.application.school.schoolapp.common_data.address.service.AddressService;
import com.example.application.school.schoolapp.student.dto.StudentDto;
import com.example.application.school.schoolapp.student.model.Student;
import com.example.application.school.schoolapp.student.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private ClassEntityService classService;

    public Long createStudent(@RequestBody StudentDto dto){
        Student entity = mapDtoToJpa(dto);

        studentRepository.save(entity);
        return entity.getId();
    }

    public Student mapDtoToJpa(StudentDto dto) {

        Student entity = new Student();
        entity.setClassEntity(findClassById(dto.getClassEntityId()));
        entity.setName(dto.getName());
        entity.setLastName(dto.getLastName());
        entity.setBirthDate(DateUtils.convertString2LocalDate(dto.getBirthDate()));
        entity.setBirthPlace(dto.getBirthPlace());
        entity.setSexId(dto.getSexId());
        entity.setAddress(addressService.mapDtoToJpa(dto.getAddress()));
        entity.setAttendanceList(new ArrayList<>());
//        entity.setStudContact(new PersonContact());
        entity.setSubjectList(new ArrayList<>());

        return entity;
    }

    public StudentDto mapJpaToDto(Student entity){
        return StudentDto.builder()
        .studentId(entity.getId())
        .name(entity.getName())
        .lastName(entity.getLastName())
        .birthDate(DateUtils.convertLocalDate2String(entity.getBirthDate()))
        .birthPlace(entity.getBirthPlace())
        .sexId(entity.getSexId())
        .address(addressService.mapJpaToDto(entity.getAddress()))
        .classEntityId(entity.getClassEntity().getId())
        .build();

    }

    private ClassEntity findClassById(Long classId){
        return classService.findByClassId(classId);
    }

    public List<StudentDto> getAllStudents() {
        return studentRepository.findAll().stream().map(student -> mapJpaToDto(student)).collect(Collectors.toList());
    }

    public StudentDto getStudentByNameAndLastname(@RequestParam String name, @RequestParam String lastname) {
        StudentDto dto = new StudentDto();
        Student student = studentRepository.findStudentByNameAndLastName(name, lastname);
        if(student != null){
            dto = mapJpaToDto(student);
        }
        return dto;
    }
}
