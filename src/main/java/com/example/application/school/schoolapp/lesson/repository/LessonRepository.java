package com.example.application.school.schoolapp.lesson.repository;

import com.example.application.school.schoolapp.lesson.model.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonRepository  extends JpaRepository<Lesson, Long>{
}
