package com.example.application.school.schoolapp.grade.dto;

import com.example.application.school.base.dto.BaseDto;
import lombok.Data;

@Data
public class GradeDto extends BaseDto {
    private String login;
}
