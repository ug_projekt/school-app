package com.example.application.school.schoolapp.common_data.address.repository;

import com.example.application.school.schoolapp.common_data.address.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
