package com.example.application.school.schoolapp.class_plan.classroom.ClassroomService;

import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_entity.service.ClassEntityService;
import com.example.application.school.schoolapp.class_plan.classroom.dto.ClassroomDto;
import com.example.application.school.schoolapp.class_plan.classroom.model.Classroom;
import com.example.application.school.schoolapp.class_plan.classroom.repository.ClassroomRepository;
import com.example.application.school.schoolapp.teacher.TeacherService.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClassroomService {

    @Autowired
    private ClassroomRepository classroomRepository;

    @Autowired
    private ClassEntityService classEntityService;

    @Autowired
    private TeacherService teacherService;

    public ClassroomDto mapEntityToDto(Classroom entity){
        ClassroomDto dto = new ClassroomDto();
        dto.setNumber(entity.getNumber());
        dto.setClassEntity(classEntityService.mapJpaToDto(entity.getClassEntity()));
        dto.setStatus(entity.getStatus());
        dto.setRemark(entity.getRemark());
        dto.setTeacherId(entity.getTeacher().getId());

        return dto;
    }

    public Classroom createClassroom(ClassroomDto dto){
        return Classroom.builder()
                .number(dto.getNumber())
                .teacher(teacherService.findTeacherById(dto.getTeacherId()))
                .remark(dto.getRemark())
                .classEntity(Optional.ofNullable(
                        classEntityService.findByClassId(dto.getClassEntity().getId()))
                        .orElse(new ClassEntity()))
                .status(dto.getStatus())
                .build();

    }

    public Classroom findClassroomById(Long classroomId){
        if(classroomId != null) {
            return Optional.ofNullable(classroomRepository.findOne(classroomId)).orElse(new Classroom());
        }else {
            return null;
        }
    }

    public Classroom findClassroomByNumber(String number){
        return classroomRepository.findByNumber(number);
    }


}
