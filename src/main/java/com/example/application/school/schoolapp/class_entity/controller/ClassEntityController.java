package com.example.application.school.schoolapp.class_entity.controller;

import com.example.application.school.schoolapp.class_entity.dto.ClassEntityDto;
import com.example.application.school.schoolapp.class_entity.service.ClassEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/class")
public class ClassEntityController {

    @Autowired
    ClassEntityService classEntityService;

    @PreAuthorize("hasAnyRole('ADMIN', 'TEACHER')")
    @RequestMapping(value = "/create/{profileId}", method = RequestMethod.POST)
    public Long createClassWithProfile(@PathVariable Long profileId,
            @RequestBody ClassEntityDto classEntityDto){
        return classEntityService.createClassEntityWithProfile(profileId, classEntityDto);
    }
}
