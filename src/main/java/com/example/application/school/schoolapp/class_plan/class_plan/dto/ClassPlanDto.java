package com.example.application.school.schoolapp.class_plan.class_plan.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.lesson.dto.LessonDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ClassPlanDto extends BaseDto {
    private String className;
    private List<LessonDto> lessonList;

}
