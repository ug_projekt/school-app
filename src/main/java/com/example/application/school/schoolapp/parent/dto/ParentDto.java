package com.example.application.school.schoolapp.parent.dto;

import com.example.application.school.base.dto.BaseDto;
import lombok.Data;

@Data
public class ParentDto extends BaseDto {
    private String login;
}
