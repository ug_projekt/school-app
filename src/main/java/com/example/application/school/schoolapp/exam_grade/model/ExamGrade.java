package com.example.application.school.schoolapp.exam_grade.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.exam.model.Exam;
import com.example.application.school.schoolapp.student.model.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "exam_grade")
public class ExamGrade extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "exam_grade_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "student_id")
    private Student student;

    @OneToOne
    @JoinColumn(name = "exam_id", referencedColumnName = "exam_id")
    private Exam exam;

}
