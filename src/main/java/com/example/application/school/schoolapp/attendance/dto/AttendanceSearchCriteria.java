package com.example.application.school.schoolapp.attendance.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author Krzysztof Lakomy
 */
@Data
@Builder
public class AttendanceSearchCriteria {
	private Long studentId;
	private String lessonDate;
	private String dateStart;
	private String dateEnd;
}
