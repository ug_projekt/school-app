package com.example.application.school.schoolapp.class_plan.class_plan.repository;

import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_plan.class_plan.model.ClassPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassPlanRepository extends JpaRepository<ClassPlan, Long> {
    ClassPlan findByClassEntity(ClassEntity classEntity);
}
