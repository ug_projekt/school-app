package com.example.application.school.schoolapp.subject.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.attendance.model.Attendance;
import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_plan.class_plan.model.ClassPlan;
import com.example.application.school.schoolapp.exam.model.Exam;
import com.example.application.school.schoolapp.grade.model.Grade;
import com.example.application.school.schoolapp.student.model.Student;
import com.example.application.school.schoolapp.teacher.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "subject")
public class Subject extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subject_id", nullable = false)
    private Long subjectId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subject")
    private List<Grade> gradeTypes;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subject")
    private List<Exam> examList;

    @JoinColumn(name = "teacher_id", referencedColumnName = "teacher_id")
    private Teacher teacher;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name= "student_id", referencedColumnName = "student_id")
    private Student student;


}
