package com.example.application.school.schoolapp.student.repository;

import com.example.application.school.schoolapp.student.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

	Student findStudentByNameAndLastName(String name, String lastname);
}
