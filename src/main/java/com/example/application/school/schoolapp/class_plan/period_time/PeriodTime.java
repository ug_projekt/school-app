package com.example.application.school.schoolapp.class_plan.period_time;

public enum PeriodTime {


    FIRST(1,"1 (8:00 - 8:45)"),
    SECOND(2, "2 (9:00 - 9:45)"),
    THIRD(3, "3 (10:00 - 10:45)"),
    FOURTH(4, "4 (11:15 - 12:00)"),
    FIFTH(5, "5 (12:15 - 13:00)"),
    SIXTH(6, "5 (13:15 - 14:00)"),
    SEVENTH(7, "5 (14:15 - 15:00)"),
    EIGHTH(8, "5 (15:15 - 16:00)");

    private int timeValue;
    private String timeName;


    PeriodTime(int timeValue, String timeName) {
        this.timeName = timeName;
        this.timeValue = timeValue;
    }

    public String getTimeName() {
        return timeName;
    }

    public void setTimeName(String timeName) {
        this.timeName = timeName;
    }

    public int getTimeValue() {
        return timeValue;
    }

    public void setTimeValue(int timeValue) {
        this.timeValue = timeValue;
    }
}
