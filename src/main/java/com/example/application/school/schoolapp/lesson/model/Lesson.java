package com.example.application.school.schoolapp.lesson.model;

import com.example.application.school.schoolapp.attendance.model.Attendance;
import com.example.application.school.schoolapp.class_plan.class_plan.model.ClassPlan;
import com.example.application.school.schoolapp.class_plan.classroom.model.Classroom;
import com.example.application.school.schoolapp.class_plan.period_time.PeriodTime;
import com.example.application.school.schoolapp.class_plan.week_day.Day;
import com.example.application.school.schoolapp.subject.model.Subject;
import com.example.application.school.schoolapp.teacher.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "lesson")
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lesson_id", nullable = false)
    private Long lessonId;

    @ManyToOne
    @JoinColumn(name = "subject_id", referencedColumnName = "subject_id")
    private Subject subject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_plan_id", referencedColumnName = "plan_id")
    private ClassPlan classPlan;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "classroom_id", referencedColumnName = "classroom_id")
    private Classroom classroom;

    @Column(name ="day_id", nullable = false)
    private Day day;

    @Column(name ="period_time", nullable = false)
    private PeriodTime periodTime;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "lesson")
    private List<Attendance> attendanceList;

    @ManyToMany
    @JoinTable(name = "teacher_lesson", joinColumns = @JoinColumn(name = "lesson_id"), inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private List<Teacher> teachers;
}
