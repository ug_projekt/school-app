package com.example.application.school.schoolapp.user.controller;

import com.example.application.school.schoolapp.user.model.User;
import com.example.application.school.schoolapp.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/getByName/{name}", method = RequestMethod.GET)
    public User getUserByName(@PathVariable("name") String name){
        return userRepository.findByUsername(name);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/getBy/{userId}", method = RequestMethod.GET)
    public User getUserById(@PathVariable("userId") Long userId){
        return userRepository.findOne(userId);
    }

}
