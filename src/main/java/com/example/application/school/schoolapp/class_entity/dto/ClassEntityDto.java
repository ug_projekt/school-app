package com.example.application.school.schoolapp.class_entity.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.class_profile.model.ClassProfile;
import com.example.application.school.schoolapp.student.dto.StudentDto;
import com.example.application.school.schoolapp.student.model.Student;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ClassEntityDto extends BaseDto {

    private Long id;
    private String name;
    private Long classProfileId;
    private String classroomNumber;
    private List<StudentDto> students;
    private Long teacherId;
}
