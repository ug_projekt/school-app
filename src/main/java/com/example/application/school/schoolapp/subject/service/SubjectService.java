package com.example.application.school.schoolapp.subject.service;

import com.example.application.school.schoolapp.subject.dto.SubjectDetailsDto;
import com.example.application.school.schoolapp.subject.model.Subject;
import com.example.application.school.schoolapp.subject.repository.SubjectRepository;
import com.example.application.school.schoolapp.teacher.TeacherService.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private TeacherService teacherService;

    public List<SubjectDetailsDto> getAllSubjects(){
        return subjectRepository.findAll()
                .stream()
                .map(s -> mapEntityToDetailsDto(s)).collect(Collectors.toList());
    }

    public SubjectDetailsDto mapEntityToDetailsDto(Subject entity){
        SubjectDetailsDto dto = new SubjectDetailsDto();
        dto.setSubjectId(String.valueOf(entity.getSubjectId()));
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        return dto;
    }

    public Subject findSubjectByName(String subjectName){
        return Optional.ofNullable(subjectRepository.findSubjectByName(subjectName))
                .orElse(new Subject());
    }

    public Subject findSubjectById(Long subjectId){
        return Optional.ofNullable(subjectRepository.findOne(subjectId)).orElse(null);
    }
}
