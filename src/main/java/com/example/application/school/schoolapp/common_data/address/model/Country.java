package com.example.application.school.schoolapp.common_data.address.model;


import com.example.application.school.base.model.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "country")
public class Country extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id", nullable = false)
    private Long countryId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "country")
    private List<City> cityList;

    @Column(name = "name")
    private String name;

    @Column(name = "country_code")
    private String code;

    @Column(name = "country_type")
    private String type;

    @Override
    public String toString() {
        return "Country{" +
                "countryId=" + countryId +
                ", cityList=" + cityList +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
