package com.example.application.school.schoolapp.common_data.address.dto;


import com.example.application.school.base.dto.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CityDto extends BaseDto {
    private Long cityId;
    private Long countryId;
    private String name;
}
