package com.example.application.school.schoolapp.student.controller;

import com.example.application.school.schoolapp.attendance.dto.AttendanceDto;
import com.example.application.school.schoolapp.attendance.dto.AttendanceSearchCriteria;
import com.example.application.school.schoolapp.attendance.service.AttendanceService;
import com.example.application.school.schoolapp.student.dto.StudentDto;
import com.example.application.school.schoolapp.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private AttendanceService attendanceService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<StudentDto> getAllStudents(){

        return studentService.getAllStudents();
    }

    @RequestMapping(value = "/getStudent/{name}/{lastname}", method = RequestMethod.GET)
    public StudentDto getAllStudents(@PathVariable("name") String name,
                                    @PathVariable("lastname") String lastname){
        return studentService.getStudentByNameAndLastname(name, lastname);
    }


    @PreAuthorize("hasAnyRole('ADMIN', 'TEACHER')")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Long createStudent(@RequestBody StudentDto studentDto){
        if(studentDto != null) {
            return studentService.createStudent(studentDto);
        }else{
            return null;
        }
    }

    @RequestMapping(value = "/getAllAttendances/{studentId}", method = RequestMethod.GET)
    public List<AttendanceDto> getAllStudentsAttendances(@PathVariable("studentId") Long studentId){
        return attendanceService.getListOfStudentAttendencesByStudent(studentId);
    }
    @RequestMapping(value = "/getAllAttendances/{studentId}/{status}", method = RequestMethod.GET)
    public List<AttendanceDto> getAllStudentsAttendances(@PathVariable("studentId") Long studentId,
                                                            @PathVariable("status") String status){
        return attendanceService.getListOfStudentAttendencesByStudentAndStatus(studentId, status);
    }

    @RequestMapping(value = "/getAllAttendancesBetweenDates", method = RequestMethod.POST)
    public List<AttendanceDto> getAllStudentsAttendancesBetweenDates(@RequestBody AttendanceSearchCriteria searchCriteria){
        return attendanceService.getListOfAttendanceForStudentBetweenDates(searchCriteria);
    }

    @RequestMapping(value = "/getAllAttendancesOnDate", method = RequestMethod.POST)
    public List<AttendanceDto> getAllStudentsAttendancesOnDate(@RequestBody AttendanceSearchCriteria searchCriteria){
        return attendanceService.getListOfAttendanceForStudentOnDate(searchCriteria);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'TEACHER')")
    @RequestMapping(value = "/updateStudentAttendance", method = RequestMethod.PUT)
    public void updateStudentAttendance(@RequestBody AttendanceDto attendanceDto){
        attendanceService.updateAttendance(attendanceDto);
    }
}
