package com.example.application.school.schoolapp.class_plan.classroom.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.class_entity.dto.ClassEntityDto;
import com.example.application.school.schoolapp.teacher.dto.TeacherDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClassroomDto extends BaseDto {

    private String number;
    private ClassEntityDto classEntity;
    private Boolean status;
    private String remark;
    private Long teacherId;

}
