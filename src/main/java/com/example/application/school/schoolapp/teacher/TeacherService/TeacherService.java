package com.example.application.school.schoolapp.teacher.TeacherService;
import com.example.application.school.schoolapp.class_entity.service.ClassEntityService;
import com.example.application.school.schoolapp.class_plan.classroom.ClassroomService.ClassroomService;
import com.example.application.school.schoolapp.common_data.address.model.Address;
import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_plan.classroom.model.Classroom;
import com.example.application.school.schoolapp.common_data.address.service.AddressService;
import com.example.application.school.schoolapp.common_data.person_contact.model.PersonContact;

import com.example.application.school.schoolapp.teacher.dto.TeacherDto;
import com.example.application.school.schoolapp.teacher.model.Teacher;
import com.example.application.school.schoolapp.teacher.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherService {

   @Autowired
   private TeacherRepository teacherRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private ClassEntityService classService;

    @Autowired
    private ClassroomService classroomService;

    private Teacher mapDtoToJpa(TeacherDto dto){
        Teacher entity = new Teacher();
        entity.setName(dto.getName());
        entity.setLastname(dto.getLastname());
        entity.setAddress(addressService.mapDtoToJpa(dto.getAddress()));
        entity.setClassEntity(findClassById(dto.getClassEntityId()));
        entity.setClassroom(classroomService.findClassroomById(dto.getClassroomId()));
        return entity;
    }

    private TeacherDto mapJpaToDto(Teacher entity){
        TeacherDto dto = TeacherDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .lastname(entity.getLastname())
                .classroomId(entity.getClassroom().getId())
                .classroomNumber(entity.getClassroom().getNumber())
                .build();
        if(entity.getClassEntity() != null){
            dto.setClassEntityId(entity.getClassEntity().getId());
            dto.setClassEntityName(entity.getClassEntity().getName());
        }
        if(entity.getAddress() != null){
            dto.setAddress(addressService.mapJpaToDto(entity.getAddress()));
        }
        return dto;
    }

    public Long createTeacher(@RequestBody TeacherDto dto){
        Teacher entity = mapDtoToJpa(dto);
        teacherRepository.save(entity);
        return entity.getId();
    }

    public List<TeacherDto> getTeachers(){
        return teacherRepository.findAll().stream()
                .map(t -> mapJpaToDto(t)).collect(Collectors.toList());
    }

    public void updateTeacher(@RequestBody TeacherDto newTeacherDto){
        if(newTeacherDto != null){
            Teacher oldTeacher = teacherRepository.findOne(newTeacherDto.getId());
            Teacher.builder()
                    .id(oldTeacher.getId())
                    .address(addressService.mapDtoToJpa(newTeacherDto.getAddress()))
                    .classEntity(classService.findByClassId(newTeacherDto.getClassEntityId()))
                    .build();
        }

    }

    private ClassEntity findClassById(Long classEntityId){
        return classService.findByClassId(classEntityId);
    }

    public Teacher findTeacherById(@RequestParam Long teacherId){
        if(teacherId != null) {
            return teacherRepository.findOne(teacherId);
        }else{
            return new Teacher();
        }
    }

}
