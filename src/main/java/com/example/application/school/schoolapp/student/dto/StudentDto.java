package com.example.application.school.schoolapp.student.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.class_entity.dto.ClassEntityDto;
import com.example.application.school.schoolapp.common_data.address.dto.AddressDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto extends BaseDto{
    private Long studentId;
    private String name;
    private String lastName;
    private String birthDate;
    private String birthPlace;
    private String sexId;
    private AddressDto address;
    private Long classEntityId;
}
