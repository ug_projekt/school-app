package com.example.application.school.schoolapp.attendance.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.student.dto.StudentDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceDto extends BaseDto {
    private Long attendanceId;
    private StudentDto student;
    private Long lessonId;
    private String date;
    private Boolean status;
    private String remark;


}
