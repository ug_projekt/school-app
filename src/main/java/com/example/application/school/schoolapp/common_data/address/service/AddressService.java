package com.example.application.school.schoolapp.common_data.address.service;

import com.example.application.school.schoolapp.common_data.address.dto.AddressDto;
import com.example.application.school.schoolapp.common_data.address.dto.CityDto;
import com.example.application.school.schoolapp.common_data.address.dto.CountryDto;
import com.example.application.school.schoolapp.common_data.address.model.Address;
import com.example.application.school.schoolapp.common_data.address.model.City;
import com.example.application.school.schoolapp.common_data.address.model.Country;
import com.example.application.school.schoolapp.common_data.address.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private CountryAndCityService countryAndCityService;

    public Address mapDtoToJpa(AddressDto dto){

       Country country = checkForCountry(dto.getCountry());

        return Address.builder()
                 .country(country)
                 .city(checkForCity(dto.getCity(), country))
                 .street(dto.getStreet())
                 .houseNumber(dto.getHouseNumber())
                 .status(dto.getStatus())
                 .defalutFlag(dto.getAddressType())
                 .build();
    }

    public AddressDto mapJpaToDto(Address entity){
        return AddressDto.builder()
                .addressId(entity.getAddressId())
                .country(countryAndCityService.mapEntityToDto(entity.getCountry()))
                .city(countryAndCityService.mapEntityToDto(entity.getCity()))
                .street(entity.getStreet())
                .houseNumber(entity.getHouseNumber())
                .status(entity.getStatus())
                .build();
    }


    public Long createAddress(AddressDto dto){
        Address entity = mapDtoToJpa(dto);
        addressRepository.save(entity);
        return entity.getAddressId();
    }

    private Country checkForCountry(CountryDto dto){
        Country country = Optional.ofNullable(countryAndCityService.findCountryById(dto))
                .orElse(countryAndCityService.mapDtoToJpa(dto));
        return country;
    }

    private City checkForCity(CityDto cityDto, Country conutry){
        List<City> listCity = countryAndCityService.getCitiesByCountry(conutry.getCountryId());
       City result = listCity.stream()
               .filter(c -> c.getName().equalsIgnoreCase(cityDto.getName()))
               .findAny()
               .orElse(countryAndCityService.setCountryToCity(cityDto, conutry));

       return result;
    }

}
