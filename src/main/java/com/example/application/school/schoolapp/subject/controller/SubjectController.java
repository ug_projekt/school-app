package com.example.application.school.schoolapp.subject.controller;

import com.example.application.school.schoolapp.subject.dto.SubjectDetailsDto;
import com.example.application.school.schoolapp.subject.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/subject")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @PreAuthorize("hasAnyRole('ADMIN', 'TEACHER')")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<SubjectDetailsDto> getAllSubjectsDetails(){
        return subjectService.getAllSubjects();
    }
}
