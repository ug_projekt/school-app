package com.example.application.school.schoolapp.class_entity.repository;

import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassEntityRepository extends JpaRepository<ClassEntity, Long> {

    ClassEntity findById(Long classId);
}
