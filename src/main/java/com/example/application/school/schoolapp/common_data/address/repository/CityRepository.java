package com.example.application.school.schoolapp.common_data.address.repository;

import com.example.application.school.schoolapp.common_data.address.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

    City findByCityId(Long cityId);
    City findByName(String name);
}
