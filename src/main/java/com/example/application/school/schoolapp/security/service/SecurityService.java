package com.example.application.school.schoolapp.security.service;

import com.example.application.school.schoolapp.role.model.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface SecurityService {

    String findLoggedInUsername();

    void autologin(String username, String password);

    void logout(HttpServletRequest request, HttpServletResponse response);

    List<Role> getRoles();
}
