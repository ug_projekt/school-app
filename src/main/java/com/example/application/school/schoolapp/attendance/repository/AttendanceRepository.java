package com.example.application.school.schoolapp.attendance.repository;

import com.example.application.school.schoolapp.attendance.model.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Krzysztof Lakomy
 */
@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long> {

	List<Attendance> findAttendanceByStudentId(Long studentId);

	List<Attendance> findAttendanceByStudentIdAndStatus(Long studentId, Boolean status);

	@Query("select a from Attendance a where a.student.id = :studentId and lessonDate = :lessonDate")
	List<Attendance> findAttendanceByStudentIdAndLessonDate(
			@Param("studentId") Long studentId,
			@Param("lessonDate") LocalDate lessonDate);

	@Query("select a from Attendance a where a.student.id = :studentId and lessonDate >= :dateStart and lessonDate <= :dateEnd")
	List<Attendance> findAttendanceByStudentIdAndLessonDateBetween(
			@Param("studentId") Long studentId,
			@Param("dateStart") LocalDate dateStart,
			@Param("dateEnd") LocalDate dateEnd);

	@Transactional
	@Modifying
	@Query("update Attendance a set a.status = :status where a.student.id = :studentId and a.lessonDate = :lessonDate and a.lesson.id = :lessonId")
	void updateAttendance(
			@Param("studentId") Long studentId,
			@Param("lessonDate") LocalDate lessonDate,
			@Param("lessonId") Long lessonId,
			@Param("status") Boolean status);

}
