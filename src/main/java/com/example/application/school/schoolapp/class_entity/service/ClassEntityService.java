package com.example.application.school.schoolapp.class_entity.service;
import com.example.application.school.schoolapp.class_entity.dto.ClassEntityDto;
import com.example.application.school.schoolapp.class_plan.classroom.ClassroomService.ClassroomService;
import com.example.application.school.schoolapp.class_profile.model.ClassProfile;
import com.example.application.school.schoolapp.class_profile.service.ClassProfileService;
import com.example.application.school.schoolapp.student.service.StudentService;
import com.example.application.school.schoolapp.teacher.TeacherService.TeacherService;
import com.example.application.school.schoolapp.teacher.model.Teacher;
import com.example.application.school.schoolapp.class_plan.class_plan.model.ClassPlan;
import com.example.application.school.schoolapp.student.model.Student;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_entity.repository.ClassEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class ClassEntityService {

    @Autowired
    private ClassEntityRepository classRepository;

    @Autowired
    private ClassProfileService classProfileService;

    @Autowired
    private ClassroomService classroomService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    public ClassEntity mapDtoToJpa(ClassEntityDto dto){
        ClassEntity entity = new ClassEntity();
        if(dto.getStudents() != null &&!dto.getStudents().isEmpty()) {
            entity.setStudents(dto.getStudents().stream()
            .map(studentDto -> studentService.mapDtoToJpa(studentDto))
                    .collect(Collectors.toList()));
        }
        if(dto.getName() != null && !dto.getName().isEmpty()) {
            entity.setName(dto.getName());
        }
        if(dto.getClassroomNumber() != null && !dto.getClassroomNumber().isEmpty()){
            entity.setClassroom(classroomService.findClassroomByNumber(dto.getClassroomNumber()));
        }
        if(dto.getTeacherId() != null){

        entity.setClassTeacher(teacherService.findTeacherById(dto.getTeacherId()));
        }
        return entity;
    }

    public ClassEntityDto mapJpaToDto(ClassEntity entity){
        return ClassEntityDto.builder()
        .id(entity.getId())
        .name(entity.getName())
        .classProfileId(entity.getClassProfile().getId())
        .build();
    }

    public Long createClassEntityWithProfile(@RequestParam Long profileId,
                                             @RequestBody ClassEntityDto dto){
        ClassProfile profile = classProfileService.getById(profileId);
        ClassEntity entity = mapDtoToJpa(dto);
        entity.setClassProfile(profile);
        classRepository.save(entity);
        return entity.getId();

    }

    public ClassEntity findByClassId(@RequestParam Long classId){
        if(classId != null) {
            return Optional.ofNullable(classRepository.findOne(classId))
                    .orElse(new ClassEntity());
        }
        return null;
    }
}
