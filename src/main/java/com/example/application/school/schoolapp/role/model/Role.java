package com.example.application.school.schoolapp.role.model;


import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.user.model.User;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "role")
public class Role extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id", nullable = false)
    private Long roleId;

    @Column(name= "role_name")
    private String name;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "roles")
    private List<User> users;



}
