package com.example.application.school.schoolapp.student.model;


import com.example.application.school.schoolapp.attendance.model.Attendance;
import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.common_data.address.model.Address;
import com.example.application.school.schoolapp.common_data.person_contact.model.PersonContact;
import com.example.application.school.schoolapp.subject.model.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id", nullable = false)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name= "class_id", referencedColumnName = "class_id")
    private ClassEntity classEntity;

    @Column(name = "stud_name")
    private String name;

    @Column(name = "stud_lastname")
    private String lastName;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "birth_place")
    private String birthPlace;

    @Column(name = "sex_id")
    private String sexId;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Attendance> attendanceList;

   /* @OneToOne
    @JoinColumn(name = "person_contact_id", referencedColumnName = "contact_id")
    PersonContact studContact;*/

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Subject> subjectList;

}
