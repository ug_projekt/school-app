package com.example.application.school.schoolapp.attendance.service;

import com.example.application.school.base.DateUtiils.DateUtils;
import com.example.application.school.schoolapp.attendance.dto.AttendanceDto;
import com.example.application.school.schoolapp.attendance.model.Attendance;
import com.example.application.school.schoolapp.attendance.repository.AttendanceRepository;
import com.example.application.school.schoolapp.attendance.dto.AttendanceSearchCriteria;
import com.example.application.school.schoolapp.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Krzysztof Lakomy
 */
@Service
public class AttendanceService {

	@Autowired
	private AttendanceRepository attendanceRepository;

	@Autowired
	private StudentService studentService;

	private AttendanceDto mapEntityToDto(Attendance entity){
	    AttendanceDto attendanceDto = new AttendanceDto();
	    attendanceDto.setAttendanceId(entity.getAttendanceId());
	    attendanceDto.setStudent(studentService.mapJpaToDto(entity.getStudent()));
	    attendanceDto.setLessonId((entity.getLesson()) != null ? entity.getLesson().getLessonId() : null);
	    attendanceDto.setDate(DateUtils.convertLocalDate2String(entity.getLessonDate()));
	    attendanceDto.setStatus(entity.getStatus());
	    attendanceDto.setRemark(entity.getRemark());
	    return attendanceDto;
	}

	public  List<AttendanceDto> getListOfStudentAttendencesByStudent(@RequestParam Long studentId){
		return attendanceRepository.findAttendanceByStudentId(studentId).stream()
				.map(attendance -> mapEntityToDto(attendance)).collect(Collectors.toList());
	}

	public  List<AttendanceDto> getListOfStudentAttendencesByStudentAndStatus(@RequestParam Long studentId, @RequestParam String status){
		Boolean attendanceStatus = Boolean.valueOf(status);
		return attendanceRepository.findAttendanceByStudentIdAndStatus(studentId, attendanceStatus).stream()
				.map(attendance -> mapEntityToDto(attendance)).collect(Collectors.toList());
	}

	public List<AttendanceDto> getListOfAttendanceForStudentBetweenDates(@RequestBody AttendanceSearchCriteria searchCriteria){
		LocalDate dateStart = LocalDate.now();
		LocalDate dateEnd = LocalDate.now();
		if(searchCriteria.getDateStart() != null && !searchCriteria.getDateStart().isEmpty()) {
			 dateStart = DateUtils.convertString2LocalDate(searchCriteria.getDateStart());
		}
		if(searchCriteria.getDateEnd() != null && !searchCriteria.getDateEnd().isEmpty()) {
			 dateEnd = DateUtils.convertString2LocalDate(searchCriteria.getDateEnd());
		}
		return attendanceRepository.findAttendanceByStudentIdAndLessonDateBetween(
				searchCriteria.getStudentId(), dateStart, dateEnd).stream()
				.map(attendance -> mapEntityToDto(attendance)).collect(Collectors.toList());

	}

	public List<AttendanceDto> getListOfAttendanceForStudentOnDate(@RequestBody AttendanceSearchCriteria searchCriteria) {
		LocalDate lessonsDate = LocalDate.now();
		if (searchCriteria.getLessonDate() != null && !searchCriteria.getLessonDate().isEmpty()) {
			lessonsDate = DateUtils.convertString2LocalDate(searchCriteria.getLessonDate());
		}
		return attendanceRepository.findAttendanceByStudentIdAndLessonDate(searchCriteria.getStudentId(), lessonsDate)
				.stream().map(attendance -> mapEntityToDto(attendance)).collect(Collectors.toList());
	}

		public void updateAttendance(@RequestBody AttendanceDto dto){
			attendanceRepository.updateAttendance(dto.getStudent().getStudentId(),
					DateUtils.convertString2LocalDate(dto.getDate()), dto.getLessonId(), dto.getStatus());

	}

}
