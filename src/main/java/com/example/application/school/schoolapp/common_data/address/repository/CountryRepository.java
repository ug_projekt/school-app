package com.example.application.school.schoolapp.common_data.address.repository;

import com.example.application.school.schoolapp.common_data.address.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByCountryId(Long countryId);
    Country findByName(String name);
}
