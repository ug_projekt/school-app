package com.example.application.school.schoolapp.class_entity.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.class_plan.class_plan.model.ClassPlan;
import com.example.application.school.schoolapp.class_plan.classroom.model.Classroom;
import com.example.application.school.schoolapp.class_profile.model.ClassProfile;
import com.example.application.school.schoolapp.student.model.Student;
import com.example.application.school.schoolapp.teacher.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "class")
public class ClassEntity extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "class_id", nullable = false)
    private Long id;

    @OneToMany(mappedBy = "classEntity")
    private List<Student> students;

    @Column(name = "class_name", length = 12)
    private String name;

    @ManyToOne
    @JoinColumn(name = "class_profile_id", referencedColumnName = "profile_id")
    private ClassProfile classProfile;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "classEntity")
    private ClassPlan classPlan;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "classEntity")
    @JoinColumn(name = "class_teacher_id")
    private Teacher classTeacher;

    @OneToOne
    @JoinColumn(name = "classroom_id", referencedColumnName = "classroom_id")
    private Classroom classroom;

}
