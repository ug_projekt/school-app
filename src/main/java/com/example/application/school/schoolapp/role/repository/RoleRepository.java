package com.example.application.school.schoolapp.role.repository;

import com.example.application.school.schoolapp.role.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

}
