package com.example.application.school.schoolapp.exam.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.exam_grade.model.ExamGrade;
import com.example.application.school.schoolapp.exam_type.model.ExamType;
import com.example.application.school.schoolapp.subject.model.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "exam")
public class Exam extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "exam_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "subject_id", referencedColumnName = "subject_id")
    private Subject subject;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "exam")
    private List<ExamType> examTypeList;

    @Column(name = "exam_date")
    private LocalDate examDate;

    @OneToOne(mappedBy = "exam")
    private ExamGrade examGrade;
}
