package com.example.application.school.schoolapp.class_plan.class_plan.controller;

import com.example.application.school.schoolapp.class_plan.class_plan.dto.ClassPlanDto;
import com.example.application.school.schoolapp.class_plan.class_plan.service.ClassPlanService;
import com.example.application.school.schoolapp.class_plan.week_day.Day;
import com.example.application.school.schoolapp.lesson.dto.LessonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/classPlan")
public class ClassPlanController {

    @Autowired
    private ClassPlanService classPlanService;

    @RequestMapping(value = "/getPlan/{classId}", method = RequestMethod.GET)
    public ClassPlanDto getClassPlan(@PathVariable("classId") Long classId){
        return classPlanService.getPlanByClass(classId);
    }

    @RequestMapping(value = "/getPlan/{classId}/{dayId}")
    public ClassPlanDto getClassPlanByClassAndDay(@PathVariable("classId") Long classId,
                                                  @PathVariable("dayId") Long day){
        ClassPlanDto classPlan = classPlanService.getPlanByClass(classId);
        List<LessonDto> lessonList = new ArrayList<>();
        lessonList.add(classPlan.getLessonList().stream()
                .filter(lesson -> lesson.getDay().equals(Day.getBydayValue(day).getDayName())).findAny()
                .orElse(null));
        classPlan.setLessonList(lessonList);
        return classPlan;
    }

}
