package com.example.application.school.schoolapp.common_data.address.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.common_data.address.model.City;
import com.example.application.school.schoolapp.common_data.address.model.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto extends BaseDto {

    private Long addressId;
    private CountryDto country;
    private CityDto city;
    private String street;
    private String houseNumber;
    private String status;
    private String defalutFlag;
    private String addressType;
}
