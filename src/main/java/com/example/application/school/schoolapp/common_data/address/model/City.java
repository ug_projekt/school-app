package com.example.application.school.schoolapp.common_data.address.model;


import com.example.application.school.base.model.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "city")
public class City extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id", nullable = false)
    private Long cityId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name= "country_id", referencedColumnName = "country_id")
    private Country country;

    @Column(name = "name")
    private String name;

    @Override
    public String toString() {
        return "City{" +
                "cityId=" + cityId +
                ", country=" + country +
                ", name='" + name + '\'' +
                '}';
    }
}
