package com.example.application.school.schoolapp.common_data.address.controller;

import com.example.application.school.schoolapp.common_data.address.dto.AddressDto;
import com.example.application.school.schoolapp.common_data.address.dto.CityDto;
import com.example.application.school.schoolapp.common_data.address.dto.CountryDto;
import com.example.application.school.schoolapp.common_data.address.service.AddressService;
import com.example.application.school.schoolapp.common_data.address.service.CountryAndCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private CountryAndCityService countryAndCityService;

    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public Long createAddress(@RequestBody AddressDto addressDto){
        if(addressDto != null) {
            return addressService.createAddress(addressDto);
        }else{
            return null;
        }
    }

    @RequestMapping(value = "/country/getAll", method = RequestMethod.GET)
    public List<CountryDto> getAllCountries(){
        return countryAndCityService.getAllCountries();
    }

    @RequestMapping(value = "/city/getByCountry/{countryId}", method = RequestMethod.GET)
    public List<CityDto> getAllCitiesFromCountry(@PathVariable ("countryId") Long countryId){
        if (countryId != null){
            return countryAndCityService.getCitiesDtoByCountry(countryId);
        }else{
            return new ArrayList<>();
        }
    }
}
