package com.example.application.school.schoolapp.class_plan.class_plan.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_plan.classroom.model.Classroom;
import com.example.application.school.schoolapp.class_plan.period_time.PeriodTime;
import com.example.application.school.schoolapp.class_plan.week_day.Day;
import com.example.application.school.schoolapp.lesson.model.Lesson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "class_plan")
public class ClassPlan extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "plan_id", nullable = false)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id", nullable = false)
    private ClassEntity classEntity;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classPlan")
    private List<Lesson> lessonList;


    @Column(name = "status", nullable = false)
    private Boolean status;


    @Column(name = "remark", length = 255)
    private String remark;


}
