package com.example.application.school.schoolapp.common_data.address.dto;

import com.example.application.school.base.dto.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountryDto extends BaseDto{
    private Long countryId;
    private String name;
    private String code;
    private String type;
}
