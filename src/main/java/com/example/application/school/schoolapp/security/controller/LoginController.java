package com.example.application.school.schoolapp.security.controller;

import com.example.application.school.schoolapp.role.model.Role;
import com.example.application.school.schoolapp.security.service.SecurityService;
import com.example.application.school.schoolapp.user.model.User;
import com.example.application.school.schoolapp.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

@CrossOrigin
@RestController
public class LoginController {

    private static final String TEACHER = "TEACHER";
    private static final String PARENT = "PARENT";
    private static final String STUDENT = "STUDENT";

    private static final String DEFAULT_EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@";
    private static final String ADMIN_EMAIL_PATTERN = DEFAULT_EMAIL_PATTERN + "admin*(\\.[A-Za-z]{2,})$";
    private static final String TEACHER_EMAIL_PATTERN = DEFAULT_EMAIL_PATTERN + "teacher*(\\.[A-Za-z]{2,})$";
    private static final String PARENT_EMAIL_PATTERN = DEFAULT_EMAIL_PATTERN + "parent*(\\.[A-Za-z]{2,})$";
    private static final String STUDENT_EMAIL_PATTERN = DEFAULT_EMAIL_PATTERN + "student*(\\.[A-Za-z]{2,})$";

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public void registerUser(){

    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Long registerUser(@RequestBody @Valid User user, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            throw new UnsupportedOperationException("Error with registration, validation error");
        }
        if(userService.findByEmail(user.getEmail()) != null){
            throw new UnsupportedOperationException("Email | " + user.getEmail() + " | already registered !");
        }
        if(userService.findByUsername(user.getUsername()) != null){
            throw new UnsupportedOperationException("User | " + user.getUsername() + " | already in use !");
        }
        List<Role> role = securityService.getRoles();

        if(user.getEmail().matches(ADMIN_EMAIL_PATTERN)){
            user.setRoles(role);
        }
        else if(user.getEmail().matches(TEACHER_EMAIL_PATTERN)){
            assignRolesToUser(user, TEACHER);
        }
        else if(user.getEmail().matches(PARENT_EMAIL_PATTERN)){
            assignRolesToUser(user, PARENT);
        }
        else if(user.getEmail().matches(STUDENT_EMAIL_PATTERN)){
            assignRolesToUser(user, STUDENT);

        }else {
            throw new UnsupportedOperationException("No permitted email domain to register !");
        }

        User savedUser = userService.save(user);

        securityService.autologin(user.getUsername(), user.getPassword());
        return savedUser.getId();

    }

    @CrossOrigin
    @RequestMapping("/login")
    public Principal user(Principal principal) {
        return principal;
    }

    private void assignRolesToUser(User user, String roleName){
        List<Role> role = securityService.getRoles();

        List<Role> assignRoles = new ArrayList<>();

        Role roleToAssign = role.stream()
                .filter(r -> r.getName().equals(roleName))
                .findAny()
                .orElseThrow(UnsupportedOperationException::new);

        assignRoles.add(roleToAssign);
        user.setRoles(assignRoles);
    }

}
