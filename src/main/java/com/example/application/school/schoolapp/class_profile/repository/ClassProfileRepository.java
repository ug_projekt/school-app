package com.example.application.school.schoolapp.class_profile.repository;

import com.example.application.school.schoolapp.class_profile.model.ClassProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassProfileRepository extends JpaRepository<ClassProfile, Long> {

}
