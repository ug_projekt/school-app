package com.example.application.school.schoolapp.exam_type.dto;

import com.example.application.school.base.dto.BaseDto;
import lombok.Data;

@Data
public class ExamTypeDto extends BaseDto {
    private String login;
}
