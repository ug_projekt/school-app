package com.example.application.school.schoolapp.attendance.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.lesson.model.Lesson;
import com.example.application.school.schoolapp.student.model.Student;
import com.example.application.school.schoolapp.subject.model.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "attendance")
public class Attendance extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attendance_id", nullable = false)
    private Long attendanceId;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "lesson_id", referencedColumnName = "lesson_id")
    private Lesson lesson;

    @Column(name = "date")
    private LocalDate lessonDate;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "remark", length = 255)
    private String remark;
}
