package com.example.application.school.schoolapp.subject.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.teacher.dto.TeacherDto;
import lombok.Data;

@Data
public class SubjectDetailsDto extends BaseDto {

    private String subjectId;
    private String name;
    private String description;
    private TeacherDto teacher;

}
