package com.example.application.school.schoolapp.common_data.address.service;

import com.example.application.school.schoolapp.common_data.address.dto.CityDto;
import com.example.application.school.schoolapp.common_data.address.dto.CountryDto;
import com.example.application.school.schoolapp.common_data.address.model.City;
import com.example.application.school.schoolapp.common_data.address.model.Country;
import com.example.application.school.schoolapp.common_data.address.repository.CityRepository;
import com.example.application.school.schoolapp.common_data.address.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CountryAndCityService {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CityRepository cityRepository;

    public Country mapDtoToJpa(CountryDto dto){
        return Country.builder()
                .name(dto.getName())
                .code(dto.getCode())
                .type(dto.getType())
                .build();
    }

    public CountryDto mapEntityToDto(Country entity){
        return CountryDto.builder()
                .countryId(entity.getCountryId())
                .name(entity.getName())
                .code(entity.getCode())
                .type(entity.getType())
                .build();

    }

    public CityDto mapEntityToDto(City entity){
        return CityDto.builder()
                .cityId(entity.getCityId())
                .name(entity.getName())
                .build();
    }

    public City mapDtoToJpa(CityDto dto){
        return City.builder()
                .name(dto.getName())
                .build();
    }

    public City findCityByName(CityDto dto){
        if(dto != null) {
            return cityRepository.findByName(dto.getName());
        }else{
            return null;
        }
    }

    public Country findCountryByCountryDtoName(CountryDto dto){
        if(dto != null) {
            return countryRepository.findByName(dto.getName());
        }else{
            return null;
        }
    }

    public Country findCountryById(CountryDto dto){
        if(dto != null && dto.getCountryId() != null) {
            return countryRepository.findByCountryId(dto.getCountryId());
        }else{
            return null;
        }
    }

    public List<City> mapCityDtoListToEntity(List<CityDto> dtoList){
         List<City> cityList = new ArrayList<>();
        for(CityDto dto : dtoList){
            if(dto != null) {
                cityList.add(mapDtoToJpa(dto));
            }
        }
        return cityList;
    }
    public List<CityDto> getCitiesDtoByCountry(Long countryId){

         return mapCitiesToDtoList(getCitiesByCountry(countryId));
    }

    public City setCountryToCity(CityDto cityDto, Country country){
        City city = mapDtoToJpa(cityDto);
        city.setCountry(country);
        return city;
    }

    public List<CountryDto> getAllCountries(){
        return countryRepository.findAll().stream().map(country -> mapEntityToDto(country)).collect(Collectors.toList());
    }

    private List<CityDto> mapCitiesToDtoList(List<City> cities){
        return cities.stream().map(c-> mapEntityToDto(c)).collect(Collectors.toList());
    }

    public List<City> getCitiesByCountry(Long countryId){
        return Optional.ofNullable(countryRepository.findByCountryId(countryId).getCityList())
                .orElse(new ArrayList<>());
    }
}
