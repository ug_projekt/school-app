package com.example.application.school.schoolapp.class_plan.classroom.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.lesson.model.Lesson;
import com.example.application.school.schoolapp.teacher.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "classroom")
public class Classroom extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "classroom_id", nullable = false)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "classroom")
    @JoinColumn(name = "class_id")
    private ClassEntity classEntity;

    @Column(name = "classroom_number")
    private String number;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "remark", length = 255)
    private String remark;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classroom")
    private List<Lesson> lessons;


    @OneToOne(cascade = CascadeType.ALL, mappedBy = "classroom")
    @JoinColumn(name = "classroom_teacher_id")
    private Teacher teacher;


}
