package com.example.application.school.schoolapp.class_profile.dto;

import com.example.application.school.base.dto.BaseDto;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClassProfileDto extends BaseDto {


    private Long id;
    private String name;
    private String description;
}
