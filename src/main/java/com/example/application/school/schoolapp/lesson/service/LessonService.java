package com.example.application.school.schoolapp.lesson.service;
import com.example.application.school.schoolapp.attendance.model.Attendance;
import com.example.application.school.schoolapp.class_plan.classroom.ClassroomService.ClassroomService;
import com.example.application.school.schoolapp.class_plan.classroom.model.Classroom;
import com.example.application.school.schoolapp.class_plan.period_time.PeriodTime;
import com.example.application.school.schoolapp.class_plan.week_day.Day;
import com.example.application.school.schoolapp.subject.model.Subject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.application.school.schoolapp.lesson.dto.LessonDto;
import com.example.application.school.schoolapp.lesson.model.Lesson;
import com.example.application.school.schoolapp.student.service.StudentService;
import com.example.application.school.schoolapp.subject.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LessonService {

    @Autowired
    private StudentService studentService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private ClassroomService classroomService;


    public LessonDto mapLesson2Dto(Lesson entity){
         LessonDto lessonDto = LessonDto.builder()
                .lessonId(entity.getLessonId())
                .classNumber(entity.getClassroom().getNumber())
                .subjectName(entity.getSubject().getName())
                .day(entity.getDay().getDayName())
                .periodTime(entity.getPeriodTime().getTimeName())
                .build();
         return lessonDto;
    }

    public Lesson mapDtoToEntity(LessonDto dto){
        Lesson lesson= new Lesson();
        lesson.setLessonId(dto.getLessonId());
        lesson.setSubject(subjectService.findSubjectByName(dto.getSubjectName()));
        lesson.setClassroom(classroomService.findClassroomByNumber(dto.getClassNumber()));
        lesson.setDay(Day.valueOf(dto.getDay()));
        lesson.setPeriodTime(PeriodTime.valueOf(dto.getPeriodTime()));
        return lesson;


    }

    public List<LessonDto> createLessonDtoList(List<Lesson> lessonList){
        if(!lessonList.isEmpty()) {
            return lessonList.stream().map(lesson -> mapLesson2Dto(lesson)).collect(Collectors.toList());
        }return new ArrayList<>();
    }

}
