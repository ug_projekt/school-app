package com.example.application.school.schoolapp.lesson.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.attendance.dto.AttendanceDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class LessonDto extends BaseDto {

    private Long lessonId;
    private String subjectName;
    private String classNumber;
    private String day;
    private String periodTime;
}
