package com.example.application.school.schoolapp.teacher.dto;

import com.example.application.school.base.dto.BaseDto;
import com.example.application.school.schoolapp.class_entity.dto.ClassEntityDto;
import com.example.application.school.schoolapp.class_plan.classroom.dto.ClassroomDto;
import com.example.application.school.schoolapp.common_data.address.dto.AddressDto;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeacherDto extends BaseDto {

    private Long id;
    private String name;
    private String lastname;
    private AddressDto address;
    private Long classEntityId;
    private Long classroomId;
    private String classroomNumber;
    private String classEntityName;
}
