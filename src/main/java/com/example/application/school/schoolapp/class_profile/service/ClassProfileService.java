package com.example.application.school.schoolapp.class_profile.service;

import com.example.application.school.schoolapp.class_profile.dto.ClassProfileDto;
import com.example.application.school.schoolapp.class_profile.model.ClassProfile;
import com.example.application.school.schoolapp.class_profile.repository.ClassProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClassProfileService {

    @Autowired
    private ClassProfileRepository classProfileRepository;

    public ClassProfile mapDtoToJpa(ClassProfileDto dto){
         return ClassProfile.builder()
                 .name(dto.getName())
                 .description(dto.getDescription())
                 .build();
    }

    public ClassProfileDto mapJpaToDto(ClassProfile entity){
        return ClassProfileDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .description(entity.getDescription())
                .build();
    }

    public ClassProfile getById(@RequestParam Long id){
        return classProfileRepository.findOne(id);
    }

    public ClassProfileDto getDtoById(@RequestParam Long id){
        return mapJpaToDto(getById(id));
    }

    public List<ClassProfileDto> getAllProfiles(){
        List<ClassProfile> classProfileList = classProfileRepository.findAll();
        List<ClassProfileDto> dtoList = new ArrayList<>();
        for (ClassProfile jpa : classProfileList
             ) {
            dtoList.add(mapJpaToDto(jpa));
        }
        return dtoList;
    }

    public Long createProfile(ClassProfileDto dto){
        ClassProfile entity = mapDtoToJpa(dto);
        classProfileRepository.save(entity);
        return entity.getId();
    }


}
