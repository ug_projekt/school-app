package com.example.application.school.schoolapp.common_data.person_contact.repository;

import com.example.application.school.schoolapp.common_data.person_contact.model.PersonContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactPersonRepository extends JpaRepository<PersonContact, Long> {
}
