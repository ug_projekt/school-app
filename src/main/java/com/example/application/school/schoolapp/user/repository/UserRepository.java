package com.example.application.school.schoolapp.user.repository;

import com.example.application.school.schoolapp.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

    User findByUsername(String name);
    User findByEmail(String email);

}
