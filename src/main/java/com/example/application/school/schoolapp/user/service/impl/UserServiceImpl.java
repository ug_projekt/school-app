package com.example.application.school.schoolapp.user.service.impl;

import com.example.application.school.schoolapp.user.model.User;
import com.example.application.school.schoolapp.user.repository.UserRepository;
import com.example.application.school.schoolapp.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User save(User user) {
        userRepository.save(user);
        return findByUsername(user.getUsername());
    }
}
