package com.example.application.school.schoolapp.subject.repository;

import com.example.application.school.schoolapp.subject.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

    Subject findSubjectByName(String subjectName);
    Subject findBySubjectId(Long subjectId);
}
