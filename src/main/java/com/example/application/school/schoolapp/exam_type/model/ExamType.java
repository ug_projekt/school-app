package com.example.application.school.schoolapp.exam_type.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.exam.model.Exam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "exam_type")
public class ExamType extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "exam_type_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "exam_id", referencedColumnName = "exam_id")
    private Exam exam;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

}
