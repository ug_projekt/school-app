package com.example.application.school.schoolapp.class_plan.class_plan.service;

import com.example.application.school.schoolapp.class_entity.model.ClassEntity;
import com.example.application.school.schoolapp.class_entity.service.ClassEntityService;
import com.example.application.school.schoolapp.class_plan.class_plan.dto.ClassPlanDto;
import com.example.application.school.schoolapp.class_plan.class_plan.model.ClassPlan;
import com.example.application.school.schoolapp.class_plan.class_plan.repository.ClassPlanRepository;
import com.example.application.school.schoolapp.lesson.dto.LessonDto;
import com.example.application.school.schoolapp.lesson.model.Lesson;
import com.example.application.school.schoolapp.lesson.service.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClassPlanService {

    @Autowired
    private ClassPlanRepository classPlanRepository;

    @Autowired
    private ClassEntityService classEntityService;

    @Autowired
    private LessonService lessonService;

    public void createPlan( Long classId, List<LessonDto> lessonList){
        ClassPlan plan = new ClassPlan();
        plan.setClassEntity(classEntityService.findByClassId(classId));
        plan.setLessonList(new ArrayList<Lesson>());
        plan.setStatus(true);
        plan.setRemark("default plan");
        classPlanRepository.save(plan);

    }

    public ClassPlanDto getPlanByClass(Long classId) {
        ClassEntity classEntity = classEntityService.findByClassId(classId);
        ClassPlan plan = classPlanRepository.findByClassEntity(classEntity);
        return mapEntityToDto(plan);
    }

    private ClassPlanDto mapEntityToDto(ClassPlan plan) {
        return ClassPlanDto.builder()
                .className(plan.getClassEntity().getName())
                .lessonList(lessonService.createLessonDtoList(plan.getLessonList()))
                .build();
    }


}
