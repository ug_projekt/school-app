package com.example.application.school.schoolapp.user.service;

import com.example.application.school.schoolapp.user.model.User;

import java.util.Optional;

public interface UserService {

    User findByUsername(String username);
    User findByEmail(String email);
    User save(User user);

}
