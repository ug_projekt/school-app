package com.example.application.school.schoolapp.teacher.controller;

import com.example.application.school.schoolapp.teacher.TeacherService.TeacherService;
import com.example.application.school.schoolapp.teacher.dto.TeacherDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping(value = "/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Long createTeacher(@RequestBody TeacherDto teacherDto){
        if(teacherDto != null){
            return teacherService.createTeacher(teacherDto);
        }else{
            return null;
        }
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<TeacherDto> getTeachers(){
        return teacherService.getTeachers();
    }
}
