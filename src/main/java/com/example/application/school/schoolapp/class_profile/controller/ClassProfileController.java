package com.example.application.school.schoolapp.class_profile.controller;


import com.example.application.school.schoolapp.class_profile.dto.ClassProfileDto;
import com.example.application.school.schoolapp.class_profile.service.ClassProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/class_profile")
public class ClassProfileController {

    @Autowired
    private ClassProfileService classProfileService;

    @PreAuthorize("hasAnyRole('ADMIN', 'TEACHER')")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Long createClassProfile(@RequestBody ClassProfileDto dto){

        return classProfileService.createProfile(dto);

    }

    @RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
    public ClassProfileDto createClassProfile(@PathVariable Long id){
        return classProfileService.getDtoById(id);

    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<ClassProfileDto> createClassProfile(){

        return classProfileService.getAllProfiles();

    }

}
