package com.example.application.school.schoolapp.common_data.person_contact.model;

import com.example.application.school.base.model.BaseEntity;
import com.example.application.school.schoolapp.student.model.Student;
import com.example.application.school.schoolapp.teacher.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person_contact")
public class PersonContact extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_id", nullable = false)
    private Long contactId;

  /*  @OneToOne(mappedBy = "teacherContact")
    Teacher teacher;

    @OneToOne(mappedBy = "studContact")
    Student student;*/

    @Column(name = "main_phone_number")
    private Long mainPhoneNumber;

    @Column(name = "second_phone_number")
    private Long secondPhoneNumber;

    @Column(name = "email_address")
    private String emailAddress;


}
