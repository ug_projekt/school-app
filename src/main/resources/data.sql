
INSERT INTO role (role_name)VALUES('ADMIN');
INSERT INTO role (role_name)VALUES('TEACHER');
INSERT INTO role (role_name)VALUES('PARENT');
INSERT INTO role (role_name)VALUES('STUDENT');

INSERT INTO user(username, password, email) VALUES ('admin', '1q2w3e', 'baki@admin.com');
INSERT INTO user_role (user_id, role_id) VALUES (1,1);
INSERT INTO user_role (user_id, role_id) VALUES (1,2);
INSERT INTO user_role (user_id, role_id) VALUES (1,3);
INSERT INTO user_role (user_id, role_id) VALUES (1,4);

INSERT INTO user(username, password, email) VALUES ('tech', '1q2w3e', 'baki@teacher.com');
INSERT INTO user_role (user_id, role_id) VALUES (2,2);

INSERT INTO user(username, password, email) VALUES ('parent', '1q2w3e', 'mama@parent.com');
INSERT INTO user_role (user_id, role_id) VALUES (3,3);

INSERT INTO user(username, password, email) VALUES ('student', '1q2w3e', 'jas@student.com');
INSERT INTO user_role (user_id, role_id) VALUES (4,4);

INSERT INTO country (name, country_code, country_type) VALUES ('Polska', 'PL', '1');
INSERT INTO country (name, country_code, country_type) VALUES ('Wielka Brytania', 'GB', '2');
INSERT INTO country (name, country_code, country_type) VALUES ('Niemcy', 'DE', '2');

INSERT INTO city (country_id, name) VALUES (1, 'Gdańsk');
INSERT INTO city (country_id, name) VALUES (1, 'Gdynia');
INSERT INTO city (country_id, name) VALUES (1, 'Sopot');
INSERT INTO city (country_id, name) VALUES (1, 'Rumia');
INSERT INTO city (country_id, name) VALUES (1, 'Reda');
INSERT INTO city (country_id, name) VALUES (1, 'Wejherowo');
INSERT INTO city (country_id, name) VALUES (1, 'Pruszcz Gdański');
INSERT INTO city (country_id, name) VALUES (1, 'Warszawa');
INSERT INTO city (country_id, name) VALUES (1, 'Kraków');
INSERT INTO city (country_id, name) VALUES (1, 'Poznań');
INSERT INTO city (country_id, name) VALUES (1, 'Wrocław');
INSERT INTO city (country_id, name) VALUES (1, 'Łódź');
INSERT INTO city (country_id, name) VALUES (1, 'Szczecin');
INSERT INTO city (country_id, name) VALUES (1, 'Olsztyn');
INSERT INTO city (country_id, name) VALUES (1, 'Częstochowa');
INSERT INTO city (country_id, name) VALUES (1, 'Białystok');

INSERT INTO subject (description, name) VALUES ('Przedmiot nauk ścisłych','Matematyka');
INSERT INTO subject (description, name) VALUES ('Przedmiot humanistyczny','Język Polski');
INSERT INTO subject (description, name) VALUES ('Przedmiot o geografii świata','Geografia');
INSERT INTO subject (description, name) VALUES ('Przedmiot nauk ścisłych','Fizyka');
INSERT INTO subject (description, name) VALUES ('Przedmiot nauk ścisłych','Biologia');
INSERT INTO subject (description, name) VALUES ('Przedmiot filologii języka obcego','Język Angielski');
INSERT INTO subject (description, name) VALUES ('Przedmiot nauk ścisłych','Chemia');
INSERT INTO subject (description, name) VALUES ('Przedmiot nauk humanistycznych','Wiedza o Społeczeństwie');
INSERT INTO subject (description, name) VALUES ('Przedmiot nauk katechetycznych','Religia');
INSERT INTO subject (description, name) VALUES ('Przedmiot sportów','Wychowanie Fizyczne');
INSERT INTO subject (description, name) VALUES ('Przedmiot nauk ścisłych','Informatyka');
INSERT INTO subject (description, name) VALUES ('Przedmiot filologii języka obcego','Język Niemiecki');
INSERT INTO subject (description, name) VALUES ('Przedmiot nauk humanistycznych','Historia');

INSERT INTO class_profile (description, name) VALUES ('Klasa matematyczno fizyczna', 'Mat-Fiz');
INSERT INTO class_profile (description, name) VALUES ('Klasa matematyczno informatyczna', 'Mat-Inf');
INSERT INTO class_profile (description, name) VALUES ('Klasa matematyczno geograficzna', 'Mat-Geo');
INSERT INTO class_profile (description, name) VALUES ('Klasa językowa z rozszerzonym angielskim', 'Pol-Ang');
INSERT INTO class_profile (description, name) VALUES ('Klasa biologiczno chemiczna', 'Biol-Chem');

INSERT INTO classroom (classroom_number, status )
VALUES ('10', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('11', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('12', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('13', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('14', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('15', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('16', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('17', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('18', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('19', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('20', true);
INSERT INTO classroom (classroom_number, status )
VALUES ('21', true);


INSERT INTO class (class_name, class_profile_id, classroom_id) VALUES ('A', 1, 1);
INSERT INTO class (class_name, class_profile_id, classroom_id) VALUES ('B', 2, 2);
INSERT INTO class (class_name, class_profile_id, classroom_id) VALUES ('C', 3, 3);
INSERT INTO class (class_name, class_profile_id, classroom_id) VALUES ('D', 4, 4);
INSERT INTO class (class_name, class_profile_id, classroom_id) VALUES ('E', 2, 5);

INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 1, 'ulica1', '1', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 2, 'ulica2', '2', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 2, 'ulica3', '3', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 3, 'ulica4', '4', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 4, 'ulica5', '5', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 5, 'ulica6', '6', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 12, 'ulica7', '7', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 6, 'ulica8', '8', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 7, 'ulica9', '9', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 4, 'ulica10', '10', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 8, 'ulica11', '11', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 9, 'ulica12', '12', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 7, 'ulica13', '13', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 10, 'ulica14', '14', '1', '1');
INSERT INTO address (country_id, city_id, street, house_number, status, default_flag)
VALUES (1, 1, 'ulica15', '15', '1', '1');




INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Krzysiek', 'Lakomy', '19900715', 'Gdynia', 1);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Ala', 'Lakomy', '19900716', 'Gdynia', 2);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Krzysiek', 'Brzoza', '19900715', 'Gdynia', 3);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Bartek', 'Zab', '19900715', 'Gdynia', 3);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Gosia', 'Brzoza', '19900715', 'Gdynia',5);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Krzysiek', 'Lakomy', '19900715', 'Gdynia', 5);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Ala', 'Lakomy', '19900716', 'Gdynia', 7);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Krzysiek', 'Brzoza', '19900715', 'Gdynia', 8);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Bartek', 'Zab', '19900715', 'Gdynia', 9);
INSERT INTO student(class_id, stud_name, stud_lastname, birth_date, birth_place, address_id)
VALUES (1,'Gosia', 'Brzoza', '19900715', 'Gdynia', 10);

INSERT INTO teacher (name, lastname,class_id, classroom_id)
VALUES ('Anna', 'Dobosz', 1, 1);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Jerzy', 'Bronk', 2);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Magdalena', 'Tracz',3);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Bożena', 'Biza', 4);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Grzegorz', 'Mozerski', 5);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Kamil', 'Padrek', 6);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Aleksandra', 'Nizak', 7);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Bartosz', 'Gremacki',8);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Adam', 'Stefczyk', 9);
INSERT INTO teacher (name, lastname, classroom_id)
VALUES ('Grazyna', 'Lesner', 10);

INSERT INTO class_plan(class_id, status, remark) VALUES (1, true, 'default plan');

INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 1, 1, 0, 0);

INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 1, 1, 0, 1);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 2, 1, 0, 2);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 7, 1, 0, 3);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 8, 1, 0, 4);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 10, 1, 0, 5);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 10, 1, 0, 6);

INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 11, 1, 1, 0);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 11, 1, 1, 1);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 9, 1, 1, 2);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 6, 1, 1, 3);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 6, 1, 1, 4);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 4, 1, 1, 5);

INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 3, 1, 2, 0);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 2, 1, 2, 1);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 2, 1, 2, 2);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 12, 1, 2, 3);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 13, 1, 2, 4);

INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 13, 1, 3, 0);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 1, 1, 3, 1);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 2, 1, 3, 2);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 3, 1, 3, 3);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 10, 1, 3, 4);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 6, 1, 3, 5);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 6, 1, 3, 6);

INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 1, 1, 4, 0);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 6, 1, 4, 1);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 4, 1, 4, 2);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 11, 1, 4, 3);
INSERT INTO lesson (class_plan_id, subject_id, classroom_id, day_id, period_time)
VALUES(1, 7, 1, 4, 4);

#ATTENDANCE

#Monday
#First lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,1,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,1,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,1,20180205, 1);

#Second lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,2,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,2,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,2,20180205, 1);

#Third lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,3,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,3,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,3,20180205, 1);

#Fourth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,4,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,4,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,4,20180205, 1);

#Fith lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,5,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,5,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,5,20180205, 1);

#Sixth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,6,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,6,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,6,20180205, 1);

#Seventh lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,7,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,7,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,7,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,7,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,7,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,7,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,7,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,7,20180205, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,7,20180205, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,7,20180205, 1);


#Tuesday
#First lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,8,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,8,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,8,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,8,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,8,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,8,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,8,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,8,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,8,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,8,20180206, 1);

#Second lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,9,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,9,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,9,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,9,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,9,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,9,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,9,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,9,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,9,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,9,20180206, 1);

#Third lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,10,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,10,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,10,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,10,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,10,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,10,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,10,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,10,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,10,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,10,20180206, 1);

#Fourth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,11,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,11,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,11,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,11,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,11,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,11,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,11,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,11,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,11,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,11,20180206, 1);

#Fith lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,12,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,12,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,12,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,12,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,12,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,12,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,12,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,12,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,12,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,12,20180206, 1);

#Sixth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,13,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,13,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,13,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,13,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,13,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,13,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,13,20180206, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,13,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,13,20180206, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,13,20180206, 1);

#Wednesday
#First lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,14,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,14,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,14,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,14,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,14,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,14,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,14,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,14,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,14,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,14,20180207, 1);

#Second lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,15,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,15,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,15,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,15,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,15,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,15,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,15,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,15,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,15,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,15,20180207, 1);

#Third lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,16,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,16,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,16,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,16,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,16,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,16,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,16,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,16,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,16,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,16,20180207, 1);

#Fourth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,17,20180207, 1);

#Fifth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,17,20180207, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,17,20180207, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,17,20180207, 1);

#Thursday
#First lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,17,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,17,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,17,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,17,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,17,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,17,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,17,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,17,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,17,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,17,20180208, 1);

#Second lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,18,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,18,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,18,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,18,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,18,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,18,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,18,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,18,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,18,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,18,20180208, 1);

#Third lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,19,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,19,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,19,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,19,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,19,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,19,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,19,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,19,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,19,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,19,20180208, 1);

#Fourth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,20,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,20,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,20,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,20,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,20,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,20,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,20,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,20,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,20,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,20,20180208, 1);

#Fifth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,21,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,21,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,21,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,21,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,21,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,21,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,21,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,21,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,21,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,21,20180208, 1);

#Sixth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,22,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,22,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,22,20180208, 1);

#Seventh lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,23,20180208, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,23,20180208, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,23,20180208, 1);

#Friday
#First lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,24,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,24,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,24,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,24,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,24,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,24,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,24,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,24,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,24,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,24,20180209, 1);

#Second lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,25,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,25,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,25,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,25,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,25,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,25,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,25,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,25,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,25,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,25,20180209, 1);

#Third lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,26,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,26,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,26,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,26,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,26,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,26,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,26,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,26,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,26,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,26,20180209, 1);

#Fourth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,27,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,27,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,27,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,27,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,27,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,27,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,27,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,27,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,27,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,27,20180209, 0);

#Fifth lesson
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (1,28,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (2,28,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (3,28,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (4,28,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (5,28,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (6,28,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (7,28,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (8,28,20180209, 1);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (9,28,20180209, 0);
INSERT INTO attendance(student_id, lesson_id, date, status) VALUES (10,28,20180209, 0);
