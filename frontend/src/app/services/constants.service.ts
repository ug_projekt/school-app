import { Injectable } from '@angular/core';

@Injectable()
export class Constants {
  public static COUNTRIES = [
    {
      countryId: 1,
      name: 'Poland',
      code: '',
      type: ''
    }
  ]
  public static CITIES = [
    {
      cityId: 1,
      countryId: 1,
      name: 'Gdańsk'
    },
    {
      cityId: 2,
      countryId: 1,
      name: 'Gdynia'
    },
    {
      cityId: 3,
      countryId: 1,
      name: 'Sopot'
    },
  ]
  public static SEX = [
    {
      id: '1',
      name: 'male'
    },
    {
      id: '2',
      name: 'female'
    }
  ]

}
