import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {
  public baseUrl: string = 'http://localhost:8080/'
  public requestOpts: RequestOptions
  constructor(
    public http: Http,
    public auth: AuthenticationService
  ) { 

  }

  public mergeAuthToken = (creds?) => {
    console.log('creeeeeds', creds)
    let headers = new Headers();
    headers.append('Accept', 'application/json')
    let base64Credential: string
    if (creds) {
      base64Credential = btoa( creds.username + ':' + creds.password);
      localStorage.setItem('key', base64Credential)
    } else {
      base64Credential = localStorage.getItem('key')
    }
    headers.append("Authorization", "Basic " + base64Credential);
    console.log('base64Credential', base64Credential)

    let options = new RequestOptions();
    options.headers = headers;
    this.requestOpts = options
    return options
};

  public createStudent = (student) => {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + 'student/create', student, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public getAllStudents = () => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'student/getAll', this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public createTeacher = (teacher) => {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + 'teacher/create', teacher, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public getAllTeachers = () => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'teacher/getAll', this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public createClassProfile = (classProfile) => {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + 'class_profile/create', classProfile, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public getAllClassProfiles = () => {
    return new Promise(resolve => {
      // let headers = new Headers();
      // const jwt = localStorage.getItem('jwt');
      // if (jwt) {
      //   headers.set('authorization', `Bearer ${jwt}`);
      // }
      console.log('this.requestOpts ', this.requestOpts )
      this.http.get(this.baseUrl + 'class_profile/getAll', this.mergeAuthToken())
      .subscribe(data => {
        console.log('getAllClassProfiles', data)
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public createClass = (schoolClass) => {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + 'class/create/' + schoolClass.classProfileId, schoolClass, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  // public getAllClasses = () => {
  //   return new Promise(resolve => {
  //     this.http.get(this.baseUrl + 'class/getAll')
  //     .subscribe(data => {
  //       resolve(data.json())
  //     }, error => {
  //       resolve(error)
  //     })
  //   })
  // }

  public getClassById = (id) => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'class_profile/getById?id=' + id, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public createAddress = (address) => {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + 'address/create', address, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public getUsers = () => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'user/getUsers', this.mergeAuthToken())
      .subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public getAllCountries = () => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'address/country/getAll', this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public getCitiesByCountry = (countryId) => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'address/city/getByCountry/' + countryId, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public getClassPlan = (classId) => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'classPlan/getPlan/' + classId, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public login = (creds) => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'login', this.mergeAuthToken(creds))
      .subscribe(data => {
        let response = data.json()
        console.log(response)
        if (response.authorities.length === 4) {
          console.log('admin')
          localStorage.setItem('user', 'ROLE_ADMIN');
        } else if (response.authorities.length === 1) {
          console.log('response.authorities[0].authority)', response.authorities[0].authority)
          localStorage.setItem('user', response.authorities[0].authority);
        }
        this.auth.userRole = localStorage.getItem('user')
        this.auth.isLogged = true
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }



  public createAccount = (account) => {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + 'register', account).subscribe(data => {
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public logout = () => {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + 'logout', this.mergeAuthToken()).subscribe(data => {
        console.log(data)
        resolve(data)
      }, error => {
        resolve(error)
      })
    })
  }

  public getAllSubjects = () => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'subject/getAll', this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public getAttendences = (studentId) => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'student/getAllAttendances/' + studentId, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public getAttendencesBetweenDates = (studentId) => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'student/getAllAttendancesBetweenDates/' + studentId, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

  public getAttendencesOnDate = (studentId) => {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + 'student/getAllAttendancesOnDate/' + studentId, this.mergeAuthToken())
      .subscribe(data => {
        resolve(data.json())
      }, error => {
        resolve(error)
      })
    })
  }

}
