import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticationService {
  public isLogged:any = false
  public userRole:string
  
  constructor() {
    console.log('user', localStorage.getItem('user'))
    this.userRole = localStorage.getItem('user')
    if (this.userRole) {
      this.isLogged = true
    } else {
      this.isLogged = false
    }
    console.log('logger', this.isLogged)
   }

  public canActivate() {
    return false
  }
}
