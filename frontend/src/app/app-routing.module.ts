import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { TimetableComponent } from './components/timetable/timetable.component'
import { SubjectsComponent } from './components/subjects/subjects.component'
import { FrequencyComponent } from './components/frequency/frequency.component'
import { NotificationsComponent } from './components/notifications/notifications.component'
import { ClassesInformationComponent } from './components/classes-information/classes-information.component'
import { TeachersInformationComponent } from './components/teachers-information/teachers-information.component'
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { NewStudentComponent } from './components/admin-panel/new-student/new-student.component';
import { NewTeacherComponent } from './components/admin-panel/new-teacher/new-teacher.component';
import { NewClassComponent } from './components/admin-panel/new-class/new-class.component';
import { NewAddressComponent } from './components/admin-panel/new-address/new-address.component';
import { CreateAccountComponent } from './components/admin-panel/create-account/create-account.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'timetable',
    pathMatch: 'full'
  },
  {
    path: 'timetable',
    component: TimetableComponent,
  },
    {
    path: 'subjects',
    component: SubjectsComponent,
  },
  {
    path: 'frequency',
    component: FrequencyComponent,
  },
    {
    path: 'notifications',
    component: NotificationsComponent,
  },
    {
    path: 'classes-information',
    component: ClassesInformationComponent,
  },
  {
    path: 'teachers-information',
    component: TeachersInformationComponent,
  },
  {
    path: 'admin-panel',
    component: AdminPanelComponent,
    children: [
      {
        path: '',
        redirectTo: 'add-student',
        pathMatch: 'full'
      },
      {
        path: 'add-student',
        component: NewStudentComponent,
      },
      {
        path: 'add-teacher',
        component: NewTeacherComponent,
      },
      {
        path: 'add-class',
        component: NewClassComponent,
      },
      {
        path: 'add-address',
        component: NewAddressComponent,
      },
      {
        path: 'add-account',
        component: CreateAccountComponent,
      },
    ]
  },
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
