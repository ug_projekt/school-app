import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesInformationComponent } from './classes-information.component';

describe('ClassesInformationComponent', () => {
  let component: ClassesInformationComponent;
  let fixture: ComponentFixture<ClassesInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
