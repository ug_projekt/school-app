import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-classes-information',
  templateUrl: './classes-information.component.html',
  styleUrls: ['./classes-information.component.scss']
})
export class ClassesInformationComponent implements OnInit {
  public classes: object
  constructor(
    public api: ApiService
  ) { }

  ngOnInit() {
    this.getClasses()
  }

  public getClasses = () => {
    this.api.getAllClassProfiles().then(data => {
      console.log('data', data)
      this.classes = data
    })
  }

}
