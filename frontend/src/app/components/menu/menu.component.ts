import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
    public api: ApiService,
    public auth: AuthenticationService
  ) { }

  ngOnInit() {
  }

  public logout = () => {
    this.api.logout().then((data) => {
      if (data['status'] == '200') {
        console.log(data)
        localStorage.setItem('user', '')
        localStorage.setItem('key', '')
        this.auth.isLogged = false
      }
    })
  }
}
