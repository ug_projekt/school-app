import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachersInformationComponent } from './teachers-information.component';

describe('TeachersInformationComponent', () => {
  let component: TeachersInformationComponent;
  let fixture: ComponentFixture<TeachersInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachersInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachersInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
