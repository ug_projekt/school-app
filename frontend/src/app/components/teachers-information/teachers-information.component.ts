import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-teachers-information',
  templateUrl: './teachers-information.component.html',
  styleUrls: ['./teachers-information.component.scss']
})
export class TeachersInformationComponent implements OnInit {
  private teachers: object
  private currentTeacher: Array<any>
  constructor(
    public api: ApiService
  ) { }

  ngOnInit() {
    this.getAllTeachers()
  }

  public getAllTeachers() {
    this.api.getAllTeachers().then((data) => {
      this.teachers = data
      this.currentTeacher = this.teachers[0]
      console.log(this.teachers)
    })
  }

}
