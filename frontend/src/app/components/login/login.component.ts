import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public user: object
  constructor(
    public api: ApiService,
    public auth: AuthenticationService
  ) { }

  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    }
  }

  public login(student) {
    console.log(student)
    this.api.login(student).then((data) => {
      console.log(data)
      if (data['status'] == 200) {
        // this.api.postLogin(student).then((data) => {
          this.auth.isLogged = true
          // localStorage.setItem('isLogged', 'true')
          console.log('data2', data)
        // })
      }
    })
  }
  
}
