import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-frequency',
  templateUrl: './frequency.component.html',
  styleUrls: ['./frequency.component.scss']
})
export class FrequencyComponent implements OnInit {
  public subjects: any
  public currentSubject: any
  public students: object
  public currentStudent: object
  public attendences: object
  constructor(
    public api: ApiService
  ) { }

  ngOnInit() {
    this.getAllSubjects()
    this.getAllStudents()
    // this.getAttendences(4)
  }

  public changeCurrentSubject = (chosenSubject) => {
    this.subjects.forEach((subject) => {
      subject.label == chosenSubject? this.currentSubject = subject : ''
    })
  }

  public getAllStudents = () => {
    this.api.getAllStudents().then((data) => {
      console.log('students', data)
      this.students = data
      this.currentStudent = this.students[0]
      this.getAttendences(this.currentStudent['studentId'])
    })
  }

  public getAttendences = (studentId) => {
    this.api.getAttendences(studentId).then((data) => {
      console.log('attendeces', data)
      this.attendences = data
    })
  } 

  public getAllSubjects = () => {
    this.api.getAllSubjects().then((data) => {
      this.subjects = data
      console.log('subjects', this.subjects)
    })
  }

}
