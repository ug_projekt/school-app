import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.scss']
})
export class TimetableComponent implements OnInit {
  public classes: object
  public currentClass: any
  public timeTable: Array<any> = []
  public times: Array<any> = []
  constructor(
    public api: ApiService
  ) { }

  ngOnInit() {
    this.getClasses()
  }

  public getClasses = () => {
    this.api.getAllClassProfiles().then(data => {
          this.classes = data
          this.currentClass = this.classes[0].id
          this.getPlan(this.currentClass)
    })
  }

  public getPlan = (classId) => {
    this.api.getClassPlan(classId).then((data) => {
      console.log('data', data)
      if (data.hasOwnProperty('lessonList')) {
        this.parseTimeTable(data)
      } else {
        this.timeTable = []
        this.times = []
      }
    })
  }

  public parseTimeTable(timetable) {
    let daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
    daysOfWeek.forEach((day) => {
      let lessons = timetable.lessonList.filter((l) => {
        return l.day == day
      })
      let temp = {
        day: day,
        lessons: lessons
      }
      this.timeTable.push(temp)
    })
    this.timeTable[0].lessons.forEach((lesson) => {
      this.times.push(lesson.periodTime.split('(').pop().split(')').shift())
    })
  }

}
