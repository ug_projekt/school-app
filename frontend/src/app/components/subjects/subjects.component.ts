import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {
  subjects: any
  modelSubject: any
  currentSubject: any
  constructor(
    public api: ApiService
  ) { 
    this.getSubjects()
  }

  ngOnInit() {
  }

  public changeCurrentSubject = () => {
    this.currentSubject = this.subjects[this.currentSubject-1]
  }

  public getSubjects() {
    this.api.getAllSubjects().then((data) => {
      console.log('subjects', data)
      this.subjects = data
      this.currentSubject = this.subjects[0]
    })
  }

}
