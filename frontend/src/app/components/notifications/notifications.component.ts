import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notifications: Array<any>
  constructor() { }

  ngOnInit() {
    this.notifications = [
      {
        title: "Warning",
        content: "Lorem ipsum",
        date: "22/11/2011"
      },
      {
        title: "Opinion",
        content: "Lorem ipsum 2",
        date: "10/03/2014"
      }
    ]
  }

}
