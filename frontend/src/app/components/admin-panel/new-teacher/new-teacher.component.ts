import { Component, OnInit } from '@angular/core';
import { City, Country, Address, Teacher } from '../admin-panel.model'
import { ApiService } from '../../../services/api.service'
import { Constants } from '../../../services/constants.service';

@Component({
  selector: 'app-new-teacher',
  templateUrl: './new-teacher.component.html',
  styleUrls: ['./new-teacher.component.scss']
})
export class NewTeacherComponent implements OnInit {
  public city: City
  public country: Country
  public address: Address
  public teacher: Teacher
  public countries: any
  public cities: any
  public sex = Constants.SEX
  constructor(
    public api: ApiService
  ) { 
    this.city = new City() 
    this.country = new Country()

    this.address = new Address()
    this.address.addressId = 2
    this.address.country = this.country
    this.address.city = this.city
    this.address.street = ''
    this.address.houseNumber = ''
    this.address.status = ''
    this.address.defalutFlag = ''
    this.address.addressType = ''

    this.teacher = new Teacher()
    this.teacher.name = ''
    this.teacher.lastname = ''
    this.teacher.address = this.address
    this.teacher.teacherContactId = 1
    this.teacher.sexId = Constants.SEX[0].id
    this.teacher.classEntityId = 1
    this.teacher.classroomId = 1
  }

  ngOnInit() {
    this.getCountries()
  }

  public saveTeacher = (teacher) => {
    console.log('teacher', teacher)
    this.api.createTeacher(teacher).then((data) => {

    })
  }

  public getCountries = () => {
    this.api.getAllCountries().then((data) => {
      this.countries = data
      this.address.country = this.countries[0]
      this.getCitiesByCountry(this.address.country.countryId)
    })
  }

  public getCitiesByCountry = (id) => {
    this.api.getCitiesByCountry(id).then((data) => {
      console.log('data', data)
      this.cities = data
      this.address.city = this.cities[0]
      this.address.city.countryId = id
    })
  }

  public setCity(city) {
    console.log('city', city)
  }
}
