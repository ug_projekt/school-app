export class SchoolClass {
    name: string
    classProfileId: number
}

export class ClassProfile {
    public name: string;
    public description: string
}