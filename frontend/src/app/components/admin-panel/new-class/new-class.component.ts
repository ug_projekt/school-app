import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { SchoolClass, ClassProfile } from './new-class.model'

@Component({
  selector: 'app-new-class',
  templateUrl: './new-class.component.html',
  styleUrls: ['./new-class.component.scss']
})
export class NewClassComponent implements OnInit {
  public classProfiles: object
  public currentProfile: any
  public schoolClass: SchoolClass
  public schoolProfile
  public classProfileId: number
  constructor(
    public api: ApiService
  ) { 
    this.schoolClass = {
      name: '',
      classProfileId: 1
    }
    this.schoolProfile = {
      name: '',
      description: ''
    }
  }

  ngOnInit() {
    this.getClassProfiles()
  }

  public getClassProfiles() {
    this.api.getAllClassProfiles().then(data => {
      console.log(data)
      this.classProfiles = data
      this.currentProfile = this.classProfiles[0]
    })
  }

  public saveClass(schoolClass) {
    console.log('sql class', schoolClass)
    this.api.createClass(schoolClass).then(data => {
      console.log('data', data)
      this.schoolClass = {
        name: '',
        classProfileId: 1
      }
    })
  }

  public saveProfile(schoolProfile) {
    console.log('sql profile', schoolProfile)
    this.api.createClassProfile(schoolProfile).then(data => {
      console.log('data', data)
    })
  }

}
