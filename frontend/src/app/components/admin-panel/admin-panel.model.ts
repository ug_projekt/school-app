export class City {
    public cityId: number;
    public countryId: number;
    public name: string;
}

export class Country {
    public countryId: number;
    public name: string;
    public code: string;
    public type: string;
}

export class Address {
    public addressId: number;
    public country: Country;
    public city: City;
    public street: string;
    public houseNumber: string;
    public status: string;
    public defalutFlag: string;
    public addressType: string;
}

export class Student {
    public name: string;
    public lastName: string;
    public birthDate: string;
    public birthPlace: string;
    public sexId: string;
    public address: Address;
    public classEntityId: number;
}

export class Teacher {
    public id: number;
    public name: string;
    public lastname: string;
    public address: Address;
    public teacherContactId: number;
    public sexId: string
    public classEntityId: number;
    public classroomId: number;
}
