import { Component, OnInit } from '@angular/core';
import { City, Country, Address, Student } from '../admin-panel.model'
import { ApiService } from '../../../services/api.service'
import { Constants } from '../../../services/constants.service';

@Component({
  selector: 'app-new-student',
  templateUrl: './new-student.component.html',
  styleUrls: ['./new-student.component.scss']
})
export class NewStudentComponent implements OnInit {
  public city: City
  public country: Country
  public address: Address
  public student: Student
  public countries: any
  public cities:any
  public sex = Constants.SEX
  constructor(
    public api: ApiService
  ) {
    this.city = new City() 
    this.country = new Country()

    this.address = new Address()
    this.address.country = this.country
    this.address.city = this.city
    this.address.street = ''
    this.address.houseNumber = ''
    this.address.status = ''
    this.address.defalutFlag = ''
    this.address.addressType = ''

    this.student = new Student()
    this.student.name = ''
    this.student.lastName = ''
    this.student.birthDate = ''
    this.student.birthPlace = ''
    this.student.sexId = Constants.SEX[0].id
    this.student.address = this.address
    this.student.classEntityId = 1
  }

  ngOnInit() {
    this.getCountries()
  }

  public saveStudent = (student) => {
    console.log('student', student)
    this.api.createStudent(student).then((studentId) => {
      console.log('studentId', studentId)
    })
  }

  public getCountries = () => {
    this.api.getAllCountries().then((data) => {
      this.countries = data
      this.address.country = this.countries[0]
      this.getCitiesByCountry(this.address.country.countryId)
    })
  }

  public getCitiesByCountry = (id) => {
    this.api.getCitiesByCountry(id).then((data) => {
      console.log('data', data)
      this.cities = data
      if (this.cities.length !== 0) {
        this.address.city = this.cities[0]
        this.address.city.countryId = id
      } else {
        this.address.city.countryId = id
        this.address.city.cityId = 1
        this.address.city.name = "none"
      }
    })
  }

}
