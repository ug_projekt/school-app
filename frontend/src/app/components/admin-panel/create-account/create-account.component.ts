import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {
  public newAccount: object
  constructor(
    public api: ApiService
  ) { }

  ngOnInit() {
    this.newAccount = {
      username: '',
      email: '',
      password: ''
    }
  }

  public createAccount = (account) => {
    this.api.createAccount(account).then((data) => {
      console.log('data', data)
    })
  }

}
