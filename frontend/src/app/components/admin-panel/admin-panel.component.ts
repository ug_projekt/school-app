import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {
  constructor(
    public api: ApiService,
    public router: Router
  ) { 

  }

  ngOnInit() {
    if (localStorage.getItem('user') !== 'ROLE_ADMIN') {
      this.router.navigate(['/timetable'])
    }
    this.getAllUsers()
  }
  
  public getAllUsers() {
    this.api.getUsers().then(data => {
      console.log(data)
    })
  }

}
