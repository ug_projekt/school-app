import { Component, OnInit } from '@angular/core';
// import { Address } from './new-address.model'
import { ApiService } from '../../../services/api.service';
import { City, Country, Address } from '../admin-panel.model'
import { Constants } from '../../../services/constants.service';

@Component({
  selector: 'app-new-address',
  templateUrl: './new-address.component.html',
  styleUrls: ['./new-address.component.scss']
})
export class NewAddressComponent implements OnInit {
  public city: City
  public country: Country
  public address: Address
  public countries: any
  constructor(
    public api: ApiService,
  ) { 
    this.city = new City() 
    this.address = new Address()
    // this.address.addressId = 2
    this.address.country = this.country
    this.address.city = this.city
    this.address.street = ''
    this.address.houseNumber = ''
    this.address.status = ''
    this.address.defalutFlag = ''
    this.address.addressType = ''
  }

  ngOnInit() {
    this.getCountries()
  }

  public saveAddress(address) {
    console.log('address', address)
    this.api.createAddress(address).then(data => {
      console.log('data', data)
    })
  }

  public getCountries = () => {
    this.api.getAllCountries().then((data) => {
      this.countries = data
      this.address.country = this.countries[0]
      this.address.city.countryId = this.countries[0].countryId
    })
  }

  public setCountryId(id) {
    this.address.city.countryId = id
  }

}
