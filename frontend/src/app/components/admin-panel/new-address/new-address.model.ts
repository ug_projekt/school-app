export class Address {
    countryId: number
    cityId: number
    street: string
    houseNumber: string
    status: string
    defaultFlag: string
    // addressType: string
}