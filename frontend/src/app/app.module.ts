import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module'

import { AppComponent } from './app.component';
import { TimetableComponent } from './components/timetable/timetable.component';
import { SubjectsComponent } from './components/subjects/subjects.component';
import { FrequencyComponent } from './components/frequency/frequency.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ClassesInformationComponent } from './components/classes-information/classes-information.component';
import { TeachersInformationComponent } from './components/teachers-information/teachers-information.component';
import { MenuComponent } from './components/menu/menu.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { FormsModule } from '@angular/forms';
import { ApiService } from './services/api.service'
import { HttpModule } from '@angular/http';
import { NewStudentComponent } from './components/admin-panel/new-student/new-student.component';
import { NewTeacherComponent } from './components/admin-panel/new-teacher/new-teacher.component';
import { NewClassComponent } from './components/admin-panel/new-class/new-class.component';
import { NewAddressComponent } from './components/admin-panel/new-address/new-address.component';
import { Constants } from './services/constants.service';
import { LoginComponent } from './components/login/login.component';
import { AuthenticationService } from './services/authentication.service';
import { CreateAccountComponent } from './components/admin-panel/create-account/create-account.component';

@NgModule({
  declarations: [
    AppComponent,
    TimetableComponent,
    SubjectsComponent,
    FrequencyComponent,
    NotificationsComponent,
    ClassesInformationComponent,
    TeachersInformationComponent,
    MenuComponent,
    AdminPanelComponent,
    NewStudentComponent,
    NewTeacherComponent,
    NewClassComponent,
    NewAddressComponent,
    LoginComponent,
    CreateAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    ApiService,
    Constants,
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
