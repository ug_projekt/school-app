import { Component } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    public auth: AuthenticationService,
    public api: ApiService
  ) {

  }


}
