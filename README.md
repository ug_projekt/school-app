# Aplikacja bazy danych szkoły

## Wymagania środowiskowe projektu
### Back-end
1. baza danych MySql
1. oprogramowanie IntelliJ
1. Maven
1. JDK Java 8
### Front-end
1. Node.js LTS

## Uruchomienie aplikacji w środowisku IntelliJ
1. nalezy upewnić się, że po zainstalowaniu JAVA i Maven są zmiennymi środowiskowymi
1. import projektu na podstawie pliku pom.xml
1. utworzenie w środowisku MySql bazy danych o nazwie „SCHOOLDB”
1. ustawienie danych dostępu do bazy w pliku projektu „.../school-app/src/main/resources/application.properties”
1. uruchomienie aplikacji w programie IntelliJ. Run → Run „SchoolApplication” lub poprzez skrót „shift + F10”.

## Uruchomienie aplikacji po stronie front-endu
`npm install -g @angular/cli`
W folderze school-app/frontend w terminalu:
`npm install`
`ng serve`
Aplikacja domyślnie powinna uruchomić się na `http://localhost:4200/`

## Przykładowe credentiale
`admin 1q2w3e`,
`tech 1q2w3e`,
`parent 1q2w3e`,
`student 1q2w3e`,